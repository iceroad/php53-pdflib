#!/bin/bash
# You can override config options very easily.
version=$1
vmajor=$2
vminor=$3
vpatch=$4

#gcov='--enable-gcov'
configoptions="\
--enable-bcmath \
--with-mysqli=mysqlnd \
--with-mysql=mysqlnd \
--with-curl=/usr/local/curl \
--with-gd \
--enable-gd-native-ttf \
--enable-calendar \
--enable-exif \
--enable-ftp \
--enable-mbstring \
--enable-pcntl \
--enable-soap \
--with-pdo-mysql \
--enable-sockets \
--enable-sqlite-utf8 \
--enable-wddx \
--enable-zip \
--with-jpeg-dir=/usr/lib \
--with-zlib \
--with-openssl-dir=/usr/local/ssl \
--with-openssl=/usr/local/ssl \
--with-gettext \
--with-mcrypt \
--with-bz2 \
--enable-dba=shared --with-mhash \
--enable-sysvmsg \
--enable-fpm --with-fpm-user=www-data --with-fpm-group=www-data \
--enable-xml \
--enable-soap \
$gcov"

echo $version $vmajor $vminor $vpatch