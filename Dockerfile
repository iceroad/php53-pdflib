FROM debian:10 as openssl-build
ENV OPENSSL_VERSION="1.0.2u"

RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
      ca-certificates \
      curl \
      autoconf \
      file \
      g++ \
      gcc \
      gnupg \
      libc-dev \
      make \
      pkg-config \
      re2c \
      zlib1g-dev \
    ; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

RUN set -eux; \
    cd /tmp; \
    OPENSSL_VERSION=$OPENSSL_VERSION; \
    mkdir openssl; \
    curl -sL "https://www.openssl.org/source/openssl-$OPENSSL_VERSION.tar.gz" -o openssl.tar.gz; \
    tar -xzf openssl.tar.gz -C openssl --strip-components=1; \
    cd /tmp/openssl; \
    ./config no-ssl2 no-ssl3 zlib-dynamic -fPIC && make -j$(nproc) && make install_sw; \
    rm -rf /tmp/*

FROM debian:10 as curl-build
ENV CURL_VERSION="7.74.0"
COPY --from=openssl-build "/usr/local/ssl/" "/usr/local/ssl/"

RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
      ca-certificates \
      curl \
      autoconf \
      file \
      g++ \
      gcc \
      gnupg \
      libc-dev \
      make \
      pkg-config \
      re2c \
      zlib1g-dev \
      libnghttp2-dev \
      libpsl-dev \
      libidn2-dev \
    ; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

RUN set -eux; \
    CURL_VERSION=$CURL_VERSION; \
    cd /tmp; \
    mkdir curl; \
    curl -sL "https://curl.haxx.se/download/curl-$CURL_VERSION.tar.gz" -o curl.tar.gz; \
    tar -xzf curl.tar.gz -C curl --strip-components=1; \
    cd /tmp/curl; \
    ./configure --prefix=/usr/local/curl --disable-shared --enable-static --disable-dependency-tracking \
        --disable-symbol-hiding --enable-versioned-symbols \
        --disable-threaded-resolver --with-lber-lib=lber \
        --with-ssl=/usr/local/ssl \
        --with-nghttp2 \
        --disable-gssapi --disable-ldap --disable-ldaps --disable-libssh2 --disable-rtsp; \
    make -j$(nproc); \
    make install; \
    rm -rf /tmp/*

FROM debian:10 as php-build

# Flags for C Compiler Loader: make php 5.3 work again.
ENV LDFLAGS="-lssl -lcrypto -lstdc++" \
    PATH="/opt/phpfarm/inst/php-5.3.29/sbin:/opt/phpfarm/inst/php-5.3.29/bin:${PATH}"

COPY --from=openssl-build "/usr/local/ssl/" "/usr/local/ssl/"
COPY --from=curl-build "/usr/local/curl/" "/usr/local/curl/"
COPY options-5.3.sh /opt/custom/

RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
      ca-certificates \
      git \
      wget \
      gnupg \
      bzip2 \
      g++ \
      libc-client2007e-dev \
      libxml2-dev \
      make \
      libfreetype6-dev \
      libjpeg62-turbo-dev \
      libpng-dev \
      zlib1g-dev \
      libbz2-dev \
      libnghttp2-dev \
      libpsl-dev \
      libidn2-dev \
      libmcrypt-dev \
      default-libmysqlclient-dev \
      libfcgi-dev \
    ; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

RUN cd /opt; \
    git clone https://github.com/fpoirotte/phpfarm.git phpfarm; \
    cd /opt/phpfarm/src; \
    mv /opt/custom /opt/phpfarm

# Installation will result with an error. Because we trying to install 2 SAPI, CGI and FPM.
# CGI is default for phpfarm and it will cause error conflict.
RUN set +e; \
    cd /opt/phpfarm/src; \
    ./main.sh 5.3.29 || echo "Conflict error in build as expected"

#FPM config
COPY php-fpm.conf /opt/phpfarm/inst/php-5.3.29/etc/php-fpm.conf

# PDF lib
COPY pdf.so /opt/phpfarm/inst/php-5.3.29/lib/php/extensions/no-debug-non-zts-20090626/pdf.so

# MhSendmail for mailhog
RUN wget https://github.com/mailhog/mhsendmail/releases/download/v0.2.0/mhsendmail_linux_amd64; \
    chmod +x mhsendmail_linux_amd64; \
    mv mhsendmail_linux_amd64 /usr/bin/mhsendmail

# Override stop signal to stop process gracefully
# https://github.com/php/php-src/blob/17baa87faddc2550def3ae7314236826bc1b1398/sapi/fpm/php-fpm.8.in#L163
STOPSIGNAL SIGQUIT

WORKDIR /var/www/html

EXPOSE 9000

CMD ["php-fpm"]